" Statusline
" Mostly taken from https://github.com/airblade/dotvim/blob/dd5d7737e39aad5e24c1a4a8c0d115ff2ae7b488/vimrc#L49-L91
function! WindowNumber()
  return tabpagewinnr(tabpagenr())
endfunction

set statusline=
" modified/readonly indicator
set statusline+=%#WarningMsg#%m%r%#Comment#
set statusline+=\ 
" relative path to file
set statusline+=%#Comment#%{expand('%:h')}/
" file name
set statusline+=%#Tag#%t%#Comment#
" Line number
set statusline+=%#Comment#:%l%#Comment#
set statusline+=\ 
set statusline+=\ 
" Truncation point
set statusline+=%<
" Total line count
set statusline+=%#Comment#%L\ lines%#Comment#

" RHS
set statusline+=%=

" Column number
set statusline+=%#Comment#col:%c%#Comment#
set statusline+=\ 
set statusline+=\ 
" Buffer number
set statusline+=%#String#buf:%-3n%#Comment#
set statusline+=\ 
set statusline+=\ 
" Window number
set statusline+=%#String#win:%-3.3{WindowNumber()}%#Comment#
