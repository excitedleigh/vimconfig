# Vim settings

## Installation

```bash
ln -s (pwd) ~/.config/nvim
sh installer.sh ~/.vimpkgs
virtualenv --python=python2 ~/.nvimpy/2
virtualenv --python=python3 ~/.nvimpy/3
~/.nvimpy/2/bin/pip install neovim
~/.nvimpy/3/bin/pip install neovim
```
