
set nocompatible
set noerrorbells
" turn off MacVim bell
set t_vb=
set dir=~/.vim/swaps//

" searching
set hlsearch
set incsearch
set ignorecase
set smartcase

" stops gitgutter from breaking when using a non-POSIX-compliant shell
set shell=/bin/bash

" filetype mode on
filetype plugin indent on
syntax on
set t_Co=256
" hidden buffers on
set hidden
" ignore types
set wildignore+=*.pyc,*.egg-info,*.iml,*.o,*.obj,.git,*.class,.idea,.venv,.vfenv,*.swp,MANIFEST.in,*.egg,*.sublime-*,.DS_Store,*.tm_properties,node_modules,__pycache__,build
" ctags setup
" requires ctags to be installed from Homebrew
set tags=./.tags;/
command! Mktags !ctags -R -f ./.tags --exclude=node_modules/** --exclude=bower_components/** --exclude=build/** .
" Wrap on words, not chars
" see http://vim.wikia.com/wiki/Word_wrap_without_line_breaks
set wrap
set linebreak
set nolist
" hard wrap at 80 chars by default
" set textwidth=80
set wrapmargin=0
set display=lastline
" indentation with tabstops at 4
set autoindent
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set breakindent
set showbreak=⤷\ \ \ 
set ruler
set relativenumber
setglobal relativenumber
set scrolloff=5
set colorcolumn=80
setglobal colorcolumn=80
autocmd FileType python setlocal colorcolumn=72,79
if has("gui_running")
    " gui settings
    set guifont=Fira\ Code:h14,Fira\ Mono:h14,Fira\ Mono\ 13,Source\ Code\ Pro:h14,Source\ Code\ Pro\ 13,Menlo:h14,DejaVu\ Sans\ Mono\ 13,Menlo,DejaVu\ Sans\ Mono
    " remove scrollbars
    set guioptions-=L
    set guioptions-=l
    set guioptions-=R
    set guioptions-=r
    " remove toolbar and menubar
    set guioptions-=T
    set guioptions-=m
    " use the console for dialogs
    set guioptions+=c
    " Prevent accidentally quitting with :q
    cabbrev q <c-r>=(getcmdtype()==':' && getcmdpos()==1 ? 'close' : 'q')<CR>
end
" Make readonly buffers also non-modifiable
function! UpdateModifiable()
    if !exists("b:setmodifiable")
        let b:setmodifiable = 0
    endif
    if &readonly
        if &modifiable
            setlocal nomodifiable
            let b:setmodifiable = 1
        endif
        else
            if b:setmodifiable
            setlocal modifiable
        endif
    endif
endfunction
autocmd BufReadPost * call UpdateModifiable()
" Automatically read when changed on disk but not in Vim
set autoread
" Auto write
set autowrite
autocmd FocusLost * silent! :wa

" Basic remappings
noremap ; :
noremap t j
noremap n k
noremap s l
noremap j /<CR>
noremap k ?<CR>
let mapleader = " "
" Move between splits with <leader>H/T/N/S
nmap <silent> <leader>H :wincmd h<CR>
nmap <silent> <leader>T :wincmd j<CR>
nmap <silent> <leader>N :wincmd k<CR>
nmap <silent> <leader>S :wincmd l<CR>
" Move to a specific window with e.g. 2<leader>w
nmap <leader>w <C-w><C-w>
nmap <leader>p "+p
nmap <leader>y "+y
" Copy and paste
noremap <C-x> "+d
noremap <C-c> "+y
noremap <C-v> "+p
noremap <M-x> "+d
noremap <M-c> "+y
noremap <M-v> "+p
inoremap <C-x> <Esc>"+di
inoremap <C-c> <Esc>"+yi
inoremap <C-v> <Esc>"+pi
inoremap <M-x> <Esc>"+di
inoremap <M-c> <Esc>"+yi
inoremap <M-v> <Esc>"+pi
cnoremap <C-v> <C-r>+
cnoremap <M-v> <C-r>+

" Don't let the mouse move the cursor
" (this gets really annoying when moving back and forth between apps)
nmap <LeftMouse> <nop>

" netrw remappings
augroup netrw_dvorak_fix
    autocmd!
    autocmd filetype netrw call Fix_netrw_maps_for_dvorak()
    augroup END
function! Fix_netrw_maps_for_dvorak()
    noremap <buffer> t j
    noremap <buffer> n k
    noremap <buffer> s l
endfunction

" Neovim specific settings
if has('nvim')
    tnoremap <Esc> <C-\><C-n> " Use Esc to get out of terminal mode
    nnoremap <silent> <leader>l :terminal fish<CR>
endif

" Title
autocmd BufEnter * let &titlestring = fnamemodify(getcwd(), ":t") . " - " . expand("%:.") . " - Vim"

" Statusline
" Mostly taken from https://github.com/airblade/dotvim/blob/dd5d7737e39aad5e24c1a4a8c0d115ff2ae7b488/vimrc#L49-L91
function! WindowNumber()
  return tabpagewinnr(tabpagenr())
endfunction
function! TrailingSpaceWarning()
  if !exists("b:statline_trailing_space_warning")
    let lineno = search('\s$', 'nw')
    if lineno != 0
      let b:statline_trailing_space_warning = '[trailing:'.lineno.']'
    else
      let b:statline_trailing_space_warning = ''
    endif
  endif
  return b:statline_trailing_space_warning
endfunction

" recalculate when idle, and after saving
augroup statline_trail
  autocmd!
  autocmd cursorhold,bufwritepost * unlet! b:statline_trailing_space_warning
augroup END

set statusline=
set statusline+=%#WarningMsg#%m%r%#Comment#                          " modified, readonly
set statusline+=\ 
set statusline+=%#Comment#%{expand('%:h')}/               " relative path to file's directory
set statusline+=%#Tag#%t%#Comment#                            " file name
set statusline+=%#Comment#:%l%#Comment#                            " file name
set statusline+=\ 
set statusline+=\ 
set statusline+=%<                                 " truncate here if needed
set statusline+=%#Comment#%L\ lines%#Comment#                     " number of lines

set statusline+=%=                                 " switch to RHS

set statusline+=%#Comment#col:%c%#Comment#                      " column
set statusline+=\ 
set statusline+=\ 
set statusline+=%#String#buf:%-3n%#Comment#                      " buffer number
set statusline+=\ 
set statusline+=\ 
set statusline+=%#String#win:%-3.3{WindowNumber()}%#Comment#     " window number

" Plugins
" OS Packages required:
"   Ubuntu: apt-get install cmake build-essential ruby-dev exuberant-ctags
"   OS X:   brew install cmake
" http://vimawesome.com/
call plug#begin('~/.vim/plugged')
    " Colours
    Plug 'chriskempson/base16-vim'
    Plug 'dracula/vim'
    " TODO: figure out why this won't install
    " Plug 'imcatnoone/toothpaste'
    "Plug 'godlygeek/csapprox'
    Plug 'KevinGoodsell/vim-csexact'

    " UI
    " Plug 'bling/vim-airline'

    " Editing
    Plug 'tpope/vim-commentary'
    Plug 'tpope/vim-surround'
    Plug 'editorconfig/editorconfig-vim'
    Plug 'jiangmiao/auto-pairs'
    Plug 'hynek/vim-python-pep8-indent'

    " Navigation
	Plug 'scrooloose/nerdtree'
    " Plug 'Xuyuanp/nerdtree-git-plugin' " Was causing perf issues
    Plug 'wincent/Command-T', {'do': 'cd ruby/command-t && /System/Library/Frameworks/Ruby.framework/Versions/2.0/usr/bin/ruby extconf.rb && env ARCHFLAGS=\"-arch i386\" make'}
    Plug 'majutsushi/tagbar'
    Plug 'mileszs/ack.vim'
    " Plug 'Shougo/vimfiler.vim'
    " Plug 'Shougo/unite.vim'
    Plug 'moll/vim-bbye'
    " Plug 'justinmk/vim-sneak' " TODO: try this out
    Plug 'kshenoy/vim-signature'

    " Completion
    " Plug 'Valloric/YouCompleteMe', {'do': './install.sh --clang-completer --omnisharp-completer'}
    Plug 'docunext/closetag.vim'
    Plug 'tpope/vim-endwise'
    Plug 'SirVer/ultisnips' " Was causing perf issues with YCM
    " Plug 'Shougo/neocomplcache.vim'
    Plug 'Shougo/neocomplete.vim'
    Plug 'davidhalter/jedi-vim'

    " Source Control
    Plug 'airblade/vim-gitgutter'
    Plug 'tpope/vim-fugitive'
    Plug 'gregsexton/gitv'

    " Syntax Checking
    " Plug 'scrooloose/syntastic'

    " File Type Support
    Plug 'kchmck/vim-coffee-script'
    Plug 'mtscout6/vim-cjsx'
    Plug 'dag/vim-fish'
    " Plug 'plasticboy/vim-markdown'
    Plug 'jtratner/vim-flavored-markdown'
    Plug 'pangloss/vim-javascript'
    Plug 'mxw/vim-jsx'
    Plug 'leafgarland/typescript-vim'
    Plug 'Quramy/tsuquyomi'
    Plug 'fatih/vim-go'
    Plug 'digitaltoad/vim-pug'
call plug#end()

" Colours
set background=dark
colorscheme base16-ocean
" colorscheme dracula

" Plugin settings

" airline
let g:airline_left_sep=' '
let g:airline_right_sep=' '
set laststatus=2 " Always show statusline

augroup markdown
    au!
    au BufNewFile,BufRead *.md,*.markdown,*.mkd,*.mkdn setlocal filetype=ghmarkdown
augroup END

" UltiSnips
let g:UltiSnipsExpandTrigger="<c-n>"
let g:UltiSnipsJumpForwardTrigger="<c-s>"
let g:UltiSnipsJumpBackTrigger="<c-h>"

" Plugin remappings
" NerdTree
nnoremap <silent> <leader>t :NERDTreeToggle<CR>
let g:NERDTreeMapOpenInTab=''
let g:NERDTreeMapOpenVSplit='v'
let g:NERDTreeRespectWildIgnore=1
let g:NERDTreeQuitOnOpen=1
let g:NERDTreeMinimalUI=1
let g:NERDTreeDirArrows=1
let g:NERDTreeAutoDeleteBuffer=1

" VimFiler
" nnoremap <silent> <leader>t :VimFilerExplorer<CR>
let g:vimfiler_no_default_key_mappings=1
autocmd! FileType vimfiler call g:My_vimfiler_settings()
function! g:My_vimfiler_settings()
    nmap <buffer> <Enter> <Plug>(vimfiler_smart_l)
    nmap <buffer> t <Plug>(vimfiler_loop_cursor_down)
    nmap <buffer> n <Plug>(vimfiler_loop_cursor_up)
    nmap <buffer> s <Plug>(vimfiler_smart_l)
endfunction

" YouCompleteMe
let g:ycm_autoclose_preview_window_after_insertion = 1

" Bbye
nnoremap <silent> <leader>q :Bdelete<CR>

" Command-T
nnoremap <silent> <leader>s :CommandTTag<CR>
nnoremap <silent> <leader>f :CommandT<CR>
nnoremap <silent> <leader>b :CommandTBuffer<CR>
augroup CommandTExtension
  autocmd!
  autocmd FocusGained * CommandTFlush
  autocmd BufWritePost * CommandTFlush
augroup END

" Tagbar
nnoremap <silent> <leader>n :TagbarToggle<CR>

" Ack.vim
let g:ack_apply_qmappings=0
let g:ack_apply_lmappings=0
if executable('ag')
    let g:ackprg = 'ag --nogroup --nocolor --column'
    " ag --vimgrep isn't supported on ubuntu
endif
let g:ack_mappings = {
      \ "o": "<CR>",
      \ "O": "<CR><C-W><C-W>:ccl<CR>",
      \ "go": "<CR><C-W>j",
      \ "H": "<C-W><CR><C-W>K",
      \ "gH": "<C-W><CR><C-W>K<C-W>b",
      \ "v": "<C-W><CR><C-W>H<C-W>b<C-W>J<C-W>t",
      \ "gv": "<C-W><CR><C-W>H<C-W>b<C-W>J" }

" Git
nnoremap <silent> <leader>gs :Gstatus<CR>
nnoremap <silent> <leader>gc :Gcommit<CR>
nnoremap <silent> <leader>gh :Gbrowse<CR>
nnoremap <leader>gg :Git<Space>

" YouCompleteMe
nnoremap <silent> <leader>d :YcmCompleter GoTo<CR>

" neocomplcache
" let g:neocomplcache_enable_at_startup = 1
" let g:neocomplcache_enable_smart_case = 1
" let g:neocomplcache_enable_fuzzy_completion = 1
" let g:neocomplcache_auto_completion_start_length = 2
" let g:neocomplcache_fuzzy_completion_start_length = 2
" inoremap <expr> <tab> pumvisible() ? "\<C-n>" : "\<TAB>"
" jedi-vim
" let g:jedi#auto_initialization = 1
" let g:jedi#popup_on_dot = 0
" autocmd FileType python let b:did_ftplugin = 1
" neocomplete
let g:neocomplete#enable_at_startup = 1
let g:neocomplete#enable_auto_select = 1
function! CustomTab()
    if neocomplete#complete_common_string() != ''
        return ""
    elseif pumvisible()
        call neocomplete#close_popup()
        return neocomplete#close_popup()
    else
        call UltiSnips#ExpandSnippetOrJump()
        if g:ulti_expand_res == 0
            return "\<Tab>"
        endif 
    endif
    return ""
endfunction
inoremap <expr><Tab> CustomTab()
" inoremap <expr><Tab>
" \ neocomplete#complete_common_string() != '' ?
" \   neocomplete#complete_common_string() :
" \ pumvisible() ? neocomplete#close_popup() : "\<Tab>"
autocmd FileType python setlocal omnifunc=jedi#completions
let g:jedi#completions_enabled = 0
let g:jedi#auto_vim_configuration = 0
if !exists('g:neocomplete#force_omni_input_patterns')
  let g:neocomplete#force_omni_input_patterns = {}
endif
let g:neocomplete#force_omni_input_patterns.python = '.{2,}'

" Commands
command! Rcedit e $MYVIMRC
command! Rcreload so $MYVIMRC
command! Isort %!isort -
"command! -range Yapf %!yapf -l a:firstline-a:lastline

" Abbreviations & spell corrections
" apparently I'm really good at transposing letters in 'import'
iabbrev improt import
iabbrev miport import
iabbrev miprot import
iabbrev imprto import
iabbrev imoprt import
iabbrev ipmort import 
iabbrev ture true
iabbrev Ture True
iabbrev TRue True
iabbrev FAlse False
iabbrev frmo from
iabbrev raies raise
iabbrev acconuts accounts
iabbrev usre user
iabbrev iabberv iabbrev
iabbrev Dokcer Docker
iabbrev dokcer docker

" Spell check
set spelllang=en_au
autocmd! FileType markdown,mkd,restructuredtext,rst,html,htmldjango,gitcommit setlocal spell
