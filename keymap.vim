" Space as leader key, like Spacemacs
let mapleader = " "

" Enter command mode without shift key
noremap ; :

" Move using htns (Dvorak)
noremap t j
noremap n k
noremap s l

" Jump between search results with jk (since we don't use them for
" navigation)
noremap j /<CR>
noremap k ?<CR>

" Move to a specific window with e.g. 2<leader>w
nmap <leader>w <C-w><C-w>

" Platform-native keybindings for system clipboard
noremap <C-x> "+d
noremap <C-c> "+y
noremap <C-v> "+p
noremap <M-x> "+d
noremap <M-c> "+y
noremap <M-v> "+p
inoremap <C-x> <Esc>"+di
inoremap <C-c> <Esc>"+yi
inoremap <C-v> <Esc>"+pi
inoremap <M-x> <Esc>"+di
inoremap <M-c> <Esc>"+yi
inoremap <M-v> <Esc>"+pi
cnoremap <C-v> <C-r>+
cnoremap <M-v> <C-r>+

" Don't let the mouse move the cursor
" (this gets really annoying when moving back and forth between apps)
nmap <LeftMouse> <nop>

" Neovim specific settings
if has('nvim')
    tnoremap <Esc> <C-\><C-n> " Use Esc to get out of terminal mode
    nnoremap <silent> <leader>l :terminal fish<CR>
endif
