set runtimepath+=$HOME/config/vim/dein/repos/github.com/Shougo/dein.vim

call dein#begin($HOME . '/config/vim/dein')

" Let dein manage itself
call dein#add('Shougo/dein.vim')

" Appearance
call dein#add('chriskempson/base16-vim')

" Editing
call dein#add('editorconfig/editorconfig-vim')
call dein#add('justinmk/vim-sneak')
call dein#add('tpope/vim-surround')
call dein#add('tpope/vim-commentary')
call dein#add('jiangmiao/auto-pairs')

" Navigation
call dein#add('scrooloose/nerdtree')
" call dein#add('Shougo/unite.vim')
call dein#add('ctrlpvim/ctrlp.vim')
call dein#add('Numkil/ag.nvim')
call dein#add('moll/vim-bbye')
call dein#add('eugen0329/vim-esearch')

" Checking
call dein#add('neomake/neomake')

" Source Control
call dein#add('airblade/vim-gitgutter')
call dein#add('tpope/vim-fugitive')

" Language Support
call dein#add('hynek/vim-python-pep8-indent')
call dein#add('leafgarland/typescript-vim')
call dein#add('pangloss/vim-javascript')
call dein#add('tweekmonster/django-plus.vim')

" Completion
call dein#add('Shougo/deoplete.nvim')
call dein#add('zchee/deoplete-jedi')
call dein#add('zchee/deoplete-go', {'build': 'make'})
call dein#add('Shougo/neco-vim')
call dein#add('racer-rust/vim-racer')
call dein#add('mhartington/deoplete-typescript')
call dein#add('carlitux/deoplete-ternjs')
call dein#add('ervandew/supertab')

call dein#end()

" Install plugins on startup
if dein#check_install()
  call dein#install()
endif

" --- POST-INSTALL SETTINGS ---
" Appearance
" base16-vim
set background=dark
colorscheme base16-ocean

" Editing
" auto-pairs
au FileType htmldjango let b:AutoPairs = {"{": "}", "%": "%", "#": "#"}

" Navigation
" NerdTree
nnoremap <silent> <leader>t :NERDTreeToggle<CR>
let g:NERDTreeMapOpenInTab=''
let g:NERDTreeMapOpenVSplit='v'
let g:NERDTreeRespectWildIgnore=1
let g:NERDTreeQuitOnOpen=1
let g:NERDTreeMinimalUI=1
let g:NERDTreeDirArrows=1
let g:NERDTreeAutoDeleteBuffer=1

" Unite
" call unite#filters#matcher_default#use(['matcher_fuzzy'])
" nnoremap <silent> <leader>f :Unite -start-insert file_rec<CR>
" nnoremap <silent> <leader>b :Unite -start-insert buffer<CR>

" Ctrl-P
nnoremap <silent> <leader>f :CtrlP<CR>
nnoremap <silent> <leader>b :CtrlPBuffer<CR>

" Bbye
nnoremap <silent> <leader>q :Bdelete<CR>

" Checking
" neomake
autocmd! BufWritePost * Neomake
if filereadable('node_modules/.bin/tsc')
    let g:neomake_typescript_tsc_maker = neomake#makers#ft#typescript#tsc()
    let g:neomake_typescript_tsc_maker.exe = $PWD . '/node_modules/.bin/tsc'
    let g:neomake_tsx_tsc_maker = neomake#makers#ft#tsx#tsc()
    let g:neomake_tsx_tsc_maker.exe = $PWD . '/node_modules/.bin/tsc'
end

" Source Control
" Fugitive
nnoremap <silent> <leader>gs :Gstatus<CR>
nnoremap <silent> <leader>gc :Gcommit<CR>
nnoremap <silent> <leader>gh :Gbrowse<CR>
nnoremap <leader>gg :Git<Space>

" Completion
" deoplete.nvim
let g:deoplete#enable_at_startup = 1
call deoplete#initialize()
