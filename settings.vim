" Required settings
set nocompatible
filetype plugin indent on
set t_Co=256

" Turn off error bells in MacVim and everything else
set noerrorbells
set t_vb=

" Use special virtualenvs for Neovim
let g:python_host_prog=$HOME . '/config/vim/pyenvs/2/bin/python'
let g:python3_host_prog=$HOME . '/config/vim/pyenvs/3/bin/python'

" Don't litter project directories with swaps
set dir=~/.vim/swaps//

" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase

" Allow hidden buffers
set hidden

" Wrapping
set wrap
set linebreak
set nolist
set wrapmargin=0
set display=lastline
set breakindent
set showbreak=⤷\ \ \ 

" Ruler
set ruler
set relativenumber
setglobal relativenumber

" Scrolling
set scrolloff=5

" Column markers
set colorcolumn=80
setglobal colorcolumn=80
autocmd FileType python setlocal colorcolumn=72,79

" stops gitgutter from breaking when using a non-POSIX-compliant shell
set shell=/bin/bash

" Default indentation settings
set autoindent
set expandtab
set tabstop=4
set softtabstop=0
set shiftwidth=0
autocmd FileType javascript setlocal tabstop=2

" Prevent accidentally quitting with :q
cabbrev q <c-r>=(getcmdtype()==':' && getcmdpos()==1 ? 'close' : 'q')<CR>

" NeoVim settings
if has("nvim")
    " macOS Terminal.app does not support truecolor :(
    if $TERM_PROGRAM != "Apple_Terminal"
        set termguicolors
        let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1
        let &t_SI = "\<esc>]50;CursorShape=1\x7" " Vertical bar in insert mode
        let &t_EI = "\<esc>]50;CursorShape=0\x7" " Block in normal mode
    end
end

" NeoVim.app settings
if exists("*MacSetFont")
    call MacSetFont('Fira Code', 14)
end

" Ensure writes are picked up by Webpack's file watcher
" see https://webpack.github.io/docs/troubleshooting.html#vim
set backupcopy=yes

" Automatically write when changing focus
set autowrite
autocmd FocusLost * silent! :wa
