source $HOME/config/vim/settings.vim
source $HOME/config/vim/keymap.vim
source $HOME/config/vim/plugins.vim
source $HOME/config/vim/statusline.vim

" This needs to be last, not sure why
syntax on
